let util = require("../../utils/util.js")
const app = getApp()
let ischeck = false;

// pages/bind/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    "screenHeight": app.globalData.screenHeight,
    "show": false,
    "bind_loading": false,
    "bind_disabled": true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (ischeck) {
      ischeck = false;
      util.checkSession(app);
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    ischeck = true;
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  popup: function (e) {
    this.setData({ show: true });
  },
  popupClose: function () {
    this.setData({ show: false });
  },
  inputAuthCode: function (data) {
    this.setData({ authCode: data.detail, bind_disabled: false });
    if(data.detail==""){
      this.setData({ bind_disabled: true });
    }
  },
  selfBind: function () {
    console.log(this.data.authCode)
    this.setData({ bind_loading: true });
  }
})
