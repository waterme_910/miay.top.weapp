let util = require("../../utils/util.js")
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';

// index.js
// 获取应用实例
const app = getApp()
let toast;


Page({
  data: {
    "screenHeight": app.globalData.screenHeight
  },
  // 事件处理函数
  bindViewTap() { },
  onLoad() {
    //从这里开始吧，基础准备好了，后台加密通道已建立，从onClose函数开始业务
    toast = Toast.loading({
      duration: 0,
      message: '加载中...',
      forbidClick: true,
      loadingType: 'spinner',
      selector: '#lodding-toast',
      onClose: () => {
        //自定义
        wx.redirectTo({
          "url": "/pages/bind/index"
        })
      }
    });
  },
  onShow() {
  },
  onHide(){
  }
})

const timer = setInterval(() => {
  //网络正常
  let network_ok = app.globalData.NETWORK_OK
  //网络安全
  let network_sec = app.globalData.NETWORK_SECRET;
  //握手状态
  let hello_ok = app.globalData.HELLO_OK;
  //服务状态
  let service_ok = app.globalData.SERVICE_OK;
  
  if (network_ok != undefined && network_ok == false) {
    toast.setData({
      duration: 2,
      type: "fail",
      message: "网络异常"
    });
    clearInterval(timer);
    return;
  } 
  if (service_ok != undefined && service_ok == false) {
    toast.setData({
      duration: 2,
      type: "fail",
      message: "服务异常"
    });
    clearInterval(timer);
    return;
  } 
  if (hello_ok != undefined && hello_ok) {
    if (network_sec != undefined) {
      if (network_sec == true) {
        //会话正常
        Toast.clear();
      } else {
        //网络不安全，请检查网络
        toast.setData({
          duration: 2,
          type: "fail",
          message: "网络不安全"
        });
      }
    } else {
      toast.setData({
        duration: 2,
        type: "fail",
        message: "服务异常"
      });
    }
    clearInterval(timer);
  }

}, 300);
