# Miay.top 微信小程序开发模板

---

## 集成模块
> ColorUI UI样式
> 
> @vant/weapp 小程序开发UI框架
>
> crypto-js base64 hex aes sha 加解密组件
> 
> wxmp-rsa rsa加解密组件

## 构建
https://developers.weixin.qq.com/miniprogram/dev/devtools/npm.html

## 配置
util.js SysConstParams
- appName 后台服务app名称
- server 后台服务地址
- rsaPublicKey 会话密钥交换时加密使用的rsa公钥，格式为pkcs8，后台有对应的私钥信息

## 介绍
与Miay.top 后端开发模板集成了安全通讯

- 密钥交换
 
> 服务地址
> 
> http://ip:port/${appName}/open/common/sec/init-1.0.0
> 
> 交换步骤
> 
> 1、wx.login 获取 res.code
> 
> 2、生成16位窜随机会话密钥
> 
> 3、client_hello = aesEncrypt(res.code + "|" + sha256(res.code + session_secret))
> 
> 4、secret = rsaEncrypt(session_secret)
> 
> 5、与后台服务地址交换
> 
> 6、交换成功响应
> 
> aesDecrypt(server_hi) 获取 session_key|server_hi_mac
> 
> local_session_hi_mac = sha256(session_key + session_secret)
> 
> 判断server_hi_mac与local_session_hi_mac是否一致，一致交换成功
> 
> 7、留存session_key，用于后续会话标识标志

- 安全接口

> 服务地址
> 
> http://ip:port/${appName}/secure/${module}/${service}/${action}-${version}
> 
> s_session_key: 会话标识
> 
> s_time_stamp: 交易时间戳
> 
> s_data: aes加密json窜
> 
> s_mac: sha256(sessionKey + jsonData + timeStamp)
> 

## 开发起点

/pages/index/index.js

Toast.onClose方法处开始业务开发
